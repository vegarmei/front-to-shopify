import * as fs from 'fs';

export default function createSearchString(){
    let searchStrings: string[] = [];
    let array: string[] = fs.readFileSync('inputfil.txt').toString().replace(/\r\n/g,'\n').split('\n');

    let i = 0;
    for(let j = 0; j < (Math.ceil(array.length / 20)); j ++){
        let searchString: string = "/ProductListSizes?$filter=";
        
        for(; i < (j*20+20) ;i++){
            //console.log(`i: ${i}, array.length-1: ${array.length}`);
            if((i != j*20+19) && (i < array.length-1)){
                searchString += `GTIN eq '${array[i]}' or `
                //console.log(array[i]);
            }
            else if(i > array.length-1){
                //console.log("No more values");
            }
            else{
                searchString += `GTIN eq '${array[i]}'`;
            }
        }
        searchStrings.push(searchString);
        
    }
    return searchStrings;
}

console.log(createSearchString());
console.log(createSearchString().length);