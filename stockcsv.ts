export function brKidsCsv(csvString: string, handle: string, name: string, size: string){
    let line: string = `\r\n${handle}`;
    line += "," + name.replace(/,/g,".");
    line += ",Størrelse";
    line += "," + size;
    line += ",,,,,,,,not stocked,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    return csvString += line;
}

export function brMWCsv(csvString: string, handle: string, name: string, size: string){
    let line: string = `\r\n${handle}`;
    line += "," + name.replace(/,/g, ".");
    line += ",Størrelse";
    line += "," + size;
    line += ",,,,,,,,0,0,0,0,0,0,0,0,0";
    return csvString += line;
}