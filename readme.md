# Frontsystems til shopify

## Beskrivelse
Dette er et verktøy laget for å eksportere varer fra frontsystems, vie deres oData API, til korrekt shopify csv-format.

## Installasjon
Last ned og installer:
- nodejs
- git

1. Åpne terminal eller powershell. 
2. Naviger til en mappe hvor du vil ha programmet.
3. Skriv `git clone https://gitlab.stud.idi.ntnu.no/vegarmei/front-to-shopify.git`
4. Naviger så inn i mappen som ble lastet ned.
5. Kjør kommandoen `npm install`

6. Sett så inn api-key og subscription-key i en egen fil som heter `token.ts` som ser slik ut: 
```
export const auth = {
    headers: {
        'x-api-key': 'INSERT KEY HERE',
        'Ocp-Apim-Subscription-Key': 'INSERT KEY HERE'
    }
}
```

## Bruk
Lim inn ønskede produkters EAN i `ìnputfil.txt`, påse at det ikke er noe linjeskift nederst, lagre. Kjør så `npm start` i i terminalen. Koden vil da ta litt tid til å kjøre.
Når koden er ferdig vil det være en ny `produkter.csv`, hvor all data skal være ferdig utfylt og i riktig format for shopify. Koden generer også to csv-filer for lagerimport. En for kids og en for man woman.