import axios from 'axios'
import * as fs from 'fs';
import {brKidsCsv, brMWCsv} from './stockcsv';
import createSearchString from './search-string';
import {Prod, sorter} from './productsort';
import {auth} from './token';

axios.defaults.adapter = require("axios/lib/adapters/http");
axios.defaults.baseURL = "https://frontsystemsapis.frontsystems.no/odata";

let res: any;
let searches = createSearchString();
let products: Prod[] = [];

async function runSearch(paths: string[], index: number){
    console.log(`Running search ${index +1}`);
    let prom = await axios.get(paths[index],auth)
    .then(e => {
        e.data.value.map(e => {
            products.push(e);
        });
    })
    .catch(err => {
        console.error(err);
    }).finally(() => {
        if((paths.length - 1) == index){
            createCSV(sorter(products));
            return
        }
        else{
            runSearch(paths, index + 1);
        }
    })
}

runSearch(searches,0);

    
const header = "Handle,Title,Body (HTML),Vendor,Type,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,Image Position,Image Alt Text,Gift Card,SEO Title,SEO Description,Google Shopping / Google Product Category,Google Shopping / Gender,Google Shopping / Age Group,Google Shopping / MPN,Google Shopping / AdWords Grouping,Google Shopping / AdWords Labels,Google Shopping / Condition,Google Shopping / Custom Product,Google Shopping / Custom Label 0,Google Shopping / Custom Label 1,Google Shopping / Custom Label 2,Google Shopping / Custom Label 3,Google Shopping / Custom Label 4,Variant Image,Variant Weight Unit,Variant Tax Code,Cost per item";

let csv = header;

const empty = ","

let kidsCSVString = "Handle,Title,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,SKU,HS Code,COO,Brando Sirkus,Brando Kids City Lade,Brando Kids Lagunen,Brando Kids Amanda,Brando Kids Byporten,Brando Kids City Syd,Brando Kids Kvadrat,Brando Kids Lillestrøm,Brando Kids Triaden,Brando Kids Moa,Brando Kids Sandvika,Brando Kids Ski,Brando Kids Strømmen,Brando Kids Trondheim Torg,Brando Kids Vestkanten,Brando Kids Åsane";
let MWCSVString = "Handle,Title,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,SKU,HS Code,COO,Brando Trondheim Torg,Brando Strømmen,Brando Storkaia,Brando Sirkus,Brando Moa,Brando Jekta,Brando City Syd,Brando City Lade,Brando Byhaven";

function createCSV(res: Prod[]){
    for(let i = 0; i< res.length; i++){
        let thisProd = res[i];

        if(thisProd.GTIN.length > 0){

            //Handle
            let handle: string = `${thisProd.Number}-${thisProd.Variant? thisProd.Variant: thisProd.Color}`.replace(/ /g,"-").toLocaleLowerCase();
            let line: string = `\r\n${handle}`;

            //Title
            line += `,${thisProd.Name.replace(/,/g,".")} - ${thisProd.Color} - ${thisProd.Number}`;
            
            //Body(HTML)
            line += empty;
            
            //Vendor
            line += "," + thisProd.Brand;
            
            //Type
            line += empty;
            
            //Tags
            //Generer tags; placeholder empty
            let gender: string;
            if(thisProd.Gender == "f"){gender = "Dame";}
            else if(thisProd.Gender == "m"){gender = "Herre";}
            else if(thisProd.Gender == "j"){gender = "Jente";}
            else if(thisProd.Gender == "g"){gender = "Gutt";}
            else{gender = "Unisex";}

            let season: string;
            if(thisProd.Season == 'NOOS'){season = "NOOS";}
            else{season = `${`20`+thisProd.Season.substring(0,2)}`};
            const tagString = `"Brando,${thisProd.Brand},${gender},${season}"`;
            line += `,${tagString}`;
            
            //Published
            line += ",FALSE";
            
            //Option1 name
            line += ",Størrelse";
            
            //Hvis produktet har lengde
            if(thisProd.Label.includes("/") && !isNaN(Number(thisProd.Label.split("/")[0])) && thisProd.Brand != "NAME IT"){
                const size = thisProd.Label.split("/");
                if(Number(size[0]) >= 23 && (Number(size[0]) <= 44) && (Number(size[1]) >= 28) && (Number(size[1]) <= 36)){
                    console.log(`Omkrets: ${size[0]}, lengde: ${size[1]}`);
    
                    //Option1 value
                    line += `,${size[0]}`;
                    
                    //Option2 name
                    line += ",Lengde";
                    
                    //Option2 value
                    line += `,${size[1]}`;
                }else{
                    //Option1 value
                    line += `,${thisProd.Label}`;
                    
                    //Option2 name
                    line += empty;
                    
                    //Option2 value
                    line += empty;
                }
            }else{
                //Option1 value
                line += `,${thisProd.Label}`;
                
                //Option2 name
                line += empty;
                
                //Option2 value
                line += empty;
            };
            
            //Option3 name
            line += empty;
            
            //Option3 value
            line += empty;
            
            //Variant SKU,
            line += `${thisProd.Identity}`;
            
            //Variant Grams
            line += empty;
            
            //Variant Inventory Tracker
            line += ",shopify";
            
            //Variant Inventory Qty,
            line += empty;
            
            //Variant Inventory Policy,
            line += ",deny";
            
            //Variant Fulfillment Service,
            line += ",manual";
            
            //Variant Price,
            line += "," + thisProd.Price;
            
            //Variant Compare At Price,
            line += empty;
            
            //Variant Requires Shipping,
            line += ",TRUE";
            
            //Variant Taxable,
            line += ",TRUE";
            
            //Variant Barcode,
            line += `,${thisProd.GTIN}`;
            
            //Image Src,
            line += empty;
            
            //Image Position,
            line += empty;
            
            //Image Alt Text,
            line += empty;
            
            //Gift Card,
            line += empty;
            
            //SEO Title,
            line += empty;
            
            //SEO Description,
            line += ",Fast frakt på 49 kr og fri frakt på ordre over 1000 kr.";
            
            //Google Shopping / Google Product Category,
            line += empty;
            
            //Google Shopping / Gender,
            line += empty;
            
            //Google Shopping / Age Group,
            line += empty;
            
            //Google Shopping / MPN,
            line += empty;
            
            //Google Shopping / AdWords Grouping,
            line += empty;
            
            //Google Shopping / AdWords Labels,
            line += empty;
            
            //Google Shopping / Condition,
            line += empty;
            
            //Google Shopping / Custom Product,
            line += empty;
            
            //Google Shopping / Custom Label 0,
            line += empty;
            
            //Google Shopping / Custom Label 1,
            line += empty;
            
            //Google Shopping / Custom Label 2,
            line += empty;
            
            //Google Shopping / Custom Label 3,
            line += empty;
            
            //Google Shopping / Custom Label 4,
            line += empty;
            
            //Variant Image,
            line += empty;
            
            //Variant Weight Unit,
            line += empty;
            
            //Variant Tax Code,
            line += empty;
            
            //Cost per item
            line += `,${thisProd.Cost}`;
            
            csv += line;
            //console.log(line);

            
            MWCSVString = brMWCsv(MWCSVString, `${handle}`, thisProd.Name, thisProd.Label);
            kidsCSVString = brKidsCsv(kidsCSVString, `${handle}`, thisProd.Name, thisProd.Label);
        } else{
            console.log("No barcode");
        }
        };

    fs.writeFileSync('produkter.csv', csv);
    fs.writeFileSync('manwomanlager.csv',MWCSVString);
    fs.writeFileSync('kidslager.csv',kidsCSVString);
}
