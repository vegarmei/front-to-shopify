export interface Prod {
    Identity: number,
    GTIN: string,
    ProductId: number,
    Name: string,
    Number: string,
    Variant: string,
    GroupName: string,
    SubgroupName: string,
    Color: string,
    Season: string,
    Brand: string,
    isDiscounted: boolean,
    Tags?: string,
    Cost: string,
    Price: string,
    Upt: string,
    Gender: string,
    IsWebAvailable: boolean,
    Label: string
}

export function sorter(produkter: Prod[]){
    produkter.sort((a,b) => {
        if(a.Identity == b.Identity){
            return Number(a.Label.replace('/','')) - Number(b.Label.replace('/',''));
        }
        else{
            return Number(a.Identity) - Number(b.Identity);
        }
    });
    return produkter;
}